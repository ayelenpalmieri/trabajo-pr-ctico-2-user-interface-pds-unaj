###Trabajo Práctico 2 User Interface

#####Consideraciones a tener en cuenta

(Se realizaron modificaciones en el backend)

<u>Carga de Productos en la Base de Datos</u>

<p style="text-align: justify;">
    Las imagenes que se van a mostrar, deben esta en  
    la carpeta IMAGES y deben tener como nombre el 
    valor del codigo del producto almacenado en la DB.

    - EJEMPLO: "producto.codigo".jpg 
</p>
<p style="text-align: justify;">
    Los nombres de los productos cargados en la DB deben 
    contener una categoria antes (TIPO DE PRODUCTO).
    Para que funcione correctamente el Filtrado de Productos
    por Categoria.

    - EJEMPLO: SmartPhone - Moto One Vision 128gb 4gb RAM
               Notebook - Dell Intel i3 8gb 1tb 128gb Ssd
               Audicular - SkullCandy Riff Wireless Black
               Monitor - Asus MG279 27'' lps 144Hz Freesync
</p>

<u>Carga de Clientes en la Base de Datos</u>

<p style="text-align: justify;">
    El cliente al cual se le realiza la venta se
    encuentra moqueado en un JSON en carrito.js. Debe
    cargarse el mismo cliente en la DB.

```csharp 
-   var aCliente =
	{
	    "ClienteId": 1,
	    "DNI": "35095481",
	    "Nombre": "Ayelen",
	    "Apellido": "Palmieri",
	    "Direccion": "Av Libres 456",
	    "Telefono": "1564894562"
	}  
```
</p>

#####ULTIMA CORRECCION

<p style="text-align: justify;">
LOS CLIENTES Y LOS PRODUCTOS ESTAN PRECARGADOS
EJECUTAR LA MIGRACION Y ACTUALIZACION DE LA DB DESDE LA API

SE MODIFICARON LAS OBSERVACIONES DETALLADAS EN LA ENTREGA ANTERIOR
</p>


#####Posibles modificaciones a futuro

<p style="text-align: justify;">
    Implementar una nueva interfaz en la cual se pueda
    seleccionar al Cliente deseado para realizar la venta.
</p>
<p style="text-align: justify;">
    Implementar un Diseño Responsive.
</p>