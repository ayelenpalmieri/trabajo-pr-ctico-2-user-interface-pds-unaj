﻿$(document).ready(function () {
    PrintCargaPaginaDos()
    document.querySelector('#vaciar-carrito').addEventListener('click', vaciarCarrito);
    document.querySelector('#procesar-pedido').addEventListener('click', showAlert);
});


const listaProductos = document.querySelector('#lista-carrito tbody');
const montoAcumulativo = document.getElementById('monto');


function insertarCarrito(producto, cantidad) {
    var exist = false;
    const listaProductos = document.querySelector('#lista-carrito tbody');
    const rows = listaProductos.getElementsByTagName("tr")
    //recuperar la longitud de la lista
    const largoLista = rows.length;
    //longitud de lista +1 va ser el id de tr fila
    const idFila = largoLista + 1;
    const row = document.createElement('tr');
    //agregar atributo id a el elemento tr
    row.setAttribute('id', idFila)

    row.innerHTML = `
            <td>
                <span class="marca-producto2 my-0 font-sans-bold text-center" id="${producto.codigo}_C">${producto.codigo}</span>
            </td>
            <td>
                <span class="marca-producto my-0 font-sans-bold text-center" id="${producto.codigo}_C">${cantidad}</span>
            </td>
            <td>
                <img src = images/${producto.codigo}.jpg width = 100>
            </td>
            <td>
                <span class="marca-producto my-0 font-sans-bold text-center">${producto.marca}</span>
            </td>
            <td>
                <span class="nombre-producto my-0 font-sans-bold text-center">${producto.nombre}</span>
            </td>
            <td class="signo-pesos">
                <span>$</span>
            </td>
            <td>
                <span class="precio-producto my-0 font-sans-bold text-center">${producto.precio}</span>
            </td>
            <td>
                <a href="#" class="borrar-producto fa fa-trash fa-2x" id="${producto.codigo}" onclick="eliminarElemento(${idFila})"></a>
            </td>
    `;

    listaProductos.appendChild(row);

}

function removeDuplicates() {

    $('#lista-carrito').find('tr').each(function () {
        const $row = $(this);
        $row.nextAll('tr').each(function () {
            const $next = $(this);
            if ($row.find('td:first').text() == $next.find('td:first').text()) {
                $next.remove();
            }
        })
    });

}

function PrintCargaPaginaDos() {
    //guardo en variable lo que obtengo del localStorage de productos seleccionados en pagina 1
    var storage = localStorage.getItem("arrayProductos");
    //guardo en nueva variable el storage parseado a JSON
    storageParse = JSON.parse(storage);

    //recorro el storage para obtener cada uno de los elementos por key y los inserto al carrito
    for (const key in storageParse) {
        const element = storageParse[key];

        var count = storageParse.filter(elem => elem.productoId === element.productoId).length;

        insertarCarrito(element, count);

        removeDuplicates();

        sumaTotal();
    }
}



function eliminarElemento(row) {
    //guardo en variable lo que obtengo del localStorage de productos seleccionados en pagina 1
    var storage = localStorage.getItem("arrayProductos");
    //declaro variable index que guardara el numero de la fila - 1, ya que se debe comparar contra la
    //posicion del objeto que se encuentra en el storage
    var index = row - 1;
    //guardo en nueva variable el storage parseado a JSON
    storageParse = JSON.parse(storage);

    //utilizo la funcion filter para eliminar el elemento seleccionado del nuevo array
    storageParse = storageParse.filter(function (i) {
        if (i !== storageParse[index]) {
            return i;
        }
    })
    //elimino del localStorage lo almacenado anteriormente por key
    localStorage.removeItem("arrayProductos");
    //seteo al localStorage los nuevos objetos (array actualizado) parseandolos a tipo string
    localStorage.setItem("arrayProductos", JSON.stringify(storageParse))
    //recargo pagina
    window.location.reload();
}



function vaciarCarrito() {
    //elimino del localStorage lo almacenado anteriormente por key
    localStorage.removeItem("arrayProductos");
    localStorage.setItem("arrayProductos", "[]")
    window.location.reload();
}


function montoTotal(total) {
    const row = document.createElement('div');
    row.setAttribute('id', monto)
    row.innerHTML = `
    <span>
        Total : $ ${total}
    </span>
    `;
    //reemplaza nodo hijo por el ultimo nuevo creado, para no escribir nuevamente el text en h6 de TOTAL
    montoAcumulativo.replaceChild(row, montoAcumulativo.childNodes[0]);
}


function sumaTotal() {
    // obtenemos todas las filas del tbody
    var todasLasFilas = document.querySelectorAll("#lista-carrito tbody tr");
    var total = 0;
    // recorremos cada una de las filas
    todasLasFilas.forEach(function (event) {

        // obtenemos las columnas de cada fila
        var columnas = event.querySelectorAll("td");
        // obtenemos el valor de importe
        var importeProducto = parseFloat(columnas[6].textContent);
        var cantidad = parseFloat(columnas[1].textContent);
        // mostramos el total
        total = total + (cantidad * importeProducto);
    });
    //el tofixed acota decimales ya que toma valor desde el front
    montoTotal(total.toFixed(2));
}


var aCliente =
{
    "ClienteId": 1,
    "DNI": "35095481",
    "Nombre": "Ayelen",
    "Apellido": "Palmieri",
    "Direccion": "Av Libres 456",
    "Telefono": "1564894562"
}


function existeCliente() {
    var param = prompt("Ingrese dni de cliente:\n EL CLIENTE QUE EXISTE PARA PRUEBAS TIENE DNI\n 35095481\n 33025685\n 35485784");
    var URL = "https://localhost:44344/api/cliente/" + param;
    var response = httpGet(URL);
    if(JSON.parse(response).dni != null){
        cliente = JSON.parse(response);
        console.log(cliente);
        return JSON.parse(response);
        }
        else {
            showAlert4();
        }
    };


function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}

function objectoVenta() {
    //guardo en variable el array de productos del localstorage correspondiente a
    //los productos seleccionados en el carrito
    var aProductos = localStorage.getItem("arrayProductos");

    if (aProductos == "[]") {
        return null;
    } else {
        aProductos = JSON.parse(aProductos);
        //guardo en variable el objeto complejo que necesito para realizar la venta
        //el cual contiene un objeto cliente y un objeto producto que contiene una
        //lista de productos seleccionados
        var cliente = existeCliente();
        console.log(cliente);
        if(cliente.dni == null){
            return null;
        }
        var ob = { "Cliente": cliente, "Productos": aProductos }
        return ob;
    }
}


function sendVenta() {
    var objeto = objectoVenta();

    //creo una variable request a la que le asigno un nuevo objeto XMLHttpRequest
    var request = new XMLHttpRequest();

    if (objeto !== null) {
        //abro una nueva conexion, usando la solicitud POST a la URL del endpoint
        //esta solicitud no se mantiene
        request.open('POST', 'https://localhost:44344/api/ventas', false)
        //añado cabecera con valor necesario para pasar datos por POST
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.send(JSON.stringify(objeto));
    } else {
        showAlert2();
    }
}


//popup sweetalert2
function showAlert() {
    swal({
        title: '¿Realmente desea confirmar la compra?',
        text: "Confirme o Cancele la misma",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, realizar compra!'
    }).then((result) => {
        if (result.value) {
            swal(
                'Confirmado!',
                'Tu compra se realizo con Exito!',
                'success',
            )
            sendVenta();
        }
        else if (result.value != true) {
            swal(
                'Cancelado!',
                'Tu compra se Cancelo!',
                'error',
            )
        }
    })
}

function showAlert2() {
    swal({
        type: 'error',
        title: 'Oops...',
        text: 'Tu Carrito esta Vacio!',
    })
}

function showAlert3() {
    swal('Producto agregado al carrito')
}

function showAlert4() {
    swal('El cliente no existe!!')
}

