$(document).ready(function () {

    LoadProductos();
    document.querySelector('#btnproductoALocalStorage').addEventListener('click', productosALocalStorage);

    //AGREGO CLASE ACTIVE AL PRIMER ENLACE
    $('.category_list .category_item[category="all"]').addClass('category_item-active');


    //FILTRO POR PRODUCTOS
    $('.category_item').click(function () {
        var categoryProduct = $(this).attr('category');
        //ME VA MOSTRANDO EN LA CONSOLA DEL CHROME LO QUE VOY SELECCIONANDO
        //console.log(categoryProduct);

        //AGREGO CLASE ACTIVE AL ENLACE SELECCIONADO
        $('.category_item').removeClass('category_item-active');
        $(this).addClass('category_item-active');

        //OCULTO PRODUCTOS AL DAR CLICK EN OTRA CATEGORIA
        $('.product_item').css('transform', 'scale(0)'); //AL DAR CLICK LA ESCALA DEL PRODUCTO SE REDUCE Y LUEGO TODOS PIERDEN EL LUGAR DONDE SE ENCONTRABAN
        function hideProduct() {
            $('.product_item').hide();
        } setTimeout(hideProduct, 400);


        //MUESTRO PRODUCTOS AL DAR CLICK EN UNA CATEGORIA
        function showProduct() {
            $('.product_item[category="' + categoryProduct + '"]').show(); //DESPUES DE ESOS 400MS LOS PRODUCTOS SE MUESTRAN EN EL LUGAR DE INICIO
            $('.product_item[category="' + categoryProduct + '"]').css('transform', 'scale(1)'); //Y LUEGO LOS MUESTRO CON UNA ANIMACION
        } setTimeout(showProduct, 400);
    });


    //MUESTRO TODOS LOS PRODUCTOS
    $('.category_item[category="all"]').click(function () {
        function showAll() {
            $('.product_item').show();
            $('.product_item').css('transform', 'scale(1)'); //LLAMO A QUE SE MUESTREN CON UNA NUEVA ANIMACION NUEVAMENTE TODOS LOS PRODUCTOS
        } setTimeout(showAll, 400); //DESPUES DE ESTE INTERVALO DE TIEMPO NUEVAMENTE
    });
});


//FUNCION CONCRETA, DENTRO SE ENCUENTRAN FUNCIONES ANONIMAS
function LoadProductos() {
    const app = document.getElementById('#01')

    //creo una variable request a la que le asigno un nuevo objeto XMLHttpRequest
    var request = new XMLHttpRequest()

    //abro una nueva conexion, usando la solicitud GET a la URL del endpoint
    //esta solicitud no se mantiene
    request.open('GET', 'https://localhost:44344/api/producto/GetProductos', false)
    request.onload = function () {

        //se comienza a acceder a los datos del JSON a partir de aqui
        var data = JSON.parse(this.response)

        if (request.status >= 200 && request.status < 400) {
            data.forEach(producto => {
                //se crea la estructura html dentro de DOM
                const productItem = document.createElement('div')
                productItem.setAttribute('class', 'product_item')

                const categoryStr = producto.nombre.split("-")[0].toString().trimEnd()
                productItem.setAttribute('category', categoryStr)
                productItem.setAttribute('id', producto.codigo)
                productItem.setAttribute('idProducto', producto.productoId)

                const card = document.createElement('div')
                card.setAttribute('class', 'card')

                const cardHeader = document.createElement('div')
                cardHeader.setAttribute('class', 'card-header')

                const h4 = document.createElement('h4')
                h4.setAttribute('class', 'my-0 font-sans-bold text-center')
                h4.textContent = producto.nombre

                const cardBody = document.createElement('div')
                cardBody.setAttribute('class', 'card-body')

                const img = document.createElement('img')

                img.src = `images/${producto.codigo}.jpg`

                const br = document.createElement('br')
                const br2 = document.createElement('br')

                const span = document.createElement('span')
                span.setAttribute('class', 'card_product-title pricing_product-card-title precio')
                span.textContent = "$" + producto.precio

                const a = document.createElement('a')
                a.setAttribute('class', 'btn btn-success btn-block agregar-carrito')
                a.setAttribute('id', producto.codigo)
                a.textContent = "COMPRAR"
                a.onclick = function () { //FUNCION ANONIMA , CALLBACKHELL
                    recuperarDatos(producto)
                    showAlert3();
                }

                app.appendChild(productItem)
                productItem.appendChild(card)
                card.appendChild(cardHeader)
                cardHeader.appendChild(h4)
                card.appendChild(cardBody)
                cardBody.appendChild(img)
                cardBody.appendChild(br)
                cardBody.appendChild(br)
                cardBody.appendChild(span)
                cardBody.appendChild(br2)
                cardBody.appendChild(a)

            })
        } else {
            console.log('error')
        }
    }

    //Send request
    request.send()

};


